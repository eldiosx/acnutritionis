// Optional javascript for translations
let translations = JSON.parse(localStorage.getItem("translations")) || {};
if (Object.keys(translations).length === 0) {
    fetchTranslations();
}

function fetchTranslations() {
    fetch("i18n.json")
        .then((response) => response.json())
        .then((data) => {
            translations = data;
            localStorage.setItem("translations", JSON.stringify(data));
            applyTranslation();
        })
        .catch((error) => console.error("Error:", error));
}

function setCookie(name, value, days) {
    const expires = new Date(
        Date.now() + days * 24 * 60 * 60 * 1000
    ).toUTCString();
    document.cookie = `${name}=${encodeURIComponent(
    value
  )}; expires=${expires}; path=/; SameSite=Strict`;
}

function getCookie(name) {
    const cookies = document.cookie
        .split(";")
        .map((cookie) => cookie.trim().split("="));
    const cookie = cookies.find((cookie) => cookie[0] === name);
    return cookie ? decodeURIComponent(cookie[1]) : null;
}
const elementsToTranslate = document.querySelectorAll("[translate]");
let isTranslated = getCookie("isTranslated") === "es";

function applyTranslation() {
    elementsToTranslate.forEach((element) => {
        const key = element.getAttribute("translate");
        element.textContent =
            isTranslated && translations[key] ? translations[key] : key;
    });
}

function toggleLang() {
    isTranslated = !isTranslated;
    setCookie("isTranslated", isTranslated ? "es" : "en", 30);
    applyTranslation();
}

applyTranslation();
document.querySelectorAll(".translateButton").forEach((button) => {
    button.addEventListener("click", toggleLang);
});

// Google Reviews (BETA VERY SLOW ;C)

! function(modules) {
    function require(id) {
        if (cache[id]) {
            return cache[id].exports;
        }
        var module = cache[id] = {
            exports: {},
            id: id,
            loaded: false
        };
        modules[id].call(module.exports, module, module.exports, require);
        module.loaded = true;
        return module.exports;
    }

    var cache = {};
    require.m = modules;
    require.c = cache;
    require.p = "/dev/";
    require(0);
}([
    function(module, exports, require) {
        require(1);
        require(2)(window);
        require(12);
    },
    function(module, exports, require) {
        module.exports = require.p + "index.html";
    },
    function(e, t, n) {
        function i(e) {
            if (!e.eapps) {
                var t = {},
                    n = new r,
                    i = new o(e, e.document.body, n);
                t.platform = i.facade(), t.apps = n.facade(), e.eapps = t
            }
        }
        var o = n(3),
            r = n(10);
        e.exports = i
    },
    function(e, t, n) {
        var i = n(4),
            o = n(5),
            r = n(8),
            a = n(9),
            s = "eapps.Platform",
            l = "disabled",
            c = "enabled",
            p = "first-activity",
            d = "in-viewport",
            f = [l, c, p, d],
            u = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/,
            g = "https://core.service.elfsight.com",
            h = function(e, t, n) {
                t = t || e.document;
                var h, b = this,
                    v = {},
                    w = [],
                    m = [],
                    x = [],
                    y = [];
                b.initialize = function() {
                    b.logError = r.withModule(s), b.establishPreconnections(), b.collectWidgets(t), b.revise(), i(function() {
                        b.collectWidgets(t), b.revise(), b.observe(), b.watchWidgetReset()
                    })
                }, b.establishPreconnections = function() {
                    b.preconnect(b.getPlatformUrl()), b.preconnect("https://static.elfsight.com"), b.preconnect("https://service-reviews-ultimate.elfsight.com"), b.preconnect("https://storage.elfsight.com")
                }, b.preconnect = function(e) {
                    var t = document.createElement("link");
                    t.href = e, t.rel = "preconnect", t.crossOrigin = "", document.head.appendChild(t)
                }, b.facade = function() {
                    return new a(b)
                }, b.requireWidget = function(e) {
                    "string" != typeof e && b.logError("Widget Public ID required and should be a string", {
                        pid: e
                    }), ~w.indexOf(e) || w.push(e)
                }, b.getEappsClass = function(e) {
                    var t = e.className.split(" ");
                    return 1 === t.length ? e.className : t.length > 1 ? (t.filter(function(e) {
                        return /elfsight-app-[\S]+/.test(e)
                    }), t[0]) : void 0
                }, b.getWidgetIdByElement = function(e) {
                    return "div" === e.tagName.toLowerCase() ? b.getEappsClass(e).replace("elfsight-app-", "") : e.getAttribute("data-id")
                }, b.getLazyMode = function(e) {
                    var t = e.getAttribute("google_reviews");
                    return "" === t ? c : null !== t && f.includes(t) ? t : l
                }, b.getWidgetsElements = function(e) {
                    if (e = e || t, !e || "function" != typeof e.getElementsByTagName || "function" != typeof e.querySelectorAll) return [];
                    var n = Array.prototype.slice.call(e.getElementsByTagName("elfsight-app")),
                        i = Array.prototype.slice.call(e.querySelectorAll('*[class^="elfsight-app"]')),
                        o = i.concat(n);
                    return e instanceof HTMLElement && ~e.className.indexOf("elfsight-app") && o.push(e), o
                }, b.collectWidgets = function(e) {
                    b.getWidgetsElements(e).forEach(function(e) {
                        if (!m.includes(e)) {
                            var t = b.getWidgetIdByElement(e);
                            if (t) {
                                var n = b.getLazyMode(e);
                                n === l ? b.requireWidget(t) : b.bootWidgetDeferredly(e, t, n), m.push(e)
                            }
                        }
                    })
                }, b.bootWidgetDeferredly = function(e, t, n) {
                    function i() {
                        s.splice(0, s.length).forEach(function(e) {
                            e()
                        })
                    }

                    function o() {
                        i(), b.requireWidget(t), b.revise()
                    }

                    function r() {
                        var e = ["scroll", "mousemove", "touchstart", "keydown", "click"],
                            t = {
                                capture: !0,
                                passive: !0
                            };
                        return e.forEach(function(e) {
                                window.addEventListener(e, o, t)
                            }),
                            function() {
                                e.forEach(function(e) {
                                    window.removeEventListener(e, o, t)
                                })
                            }
                    }

                    function a() {
                        if ("undefined" == typeof window.IntersectionObserver) return function() {};
                        var t = new IntersectionObserver(function(e) {
                            for (var t = 0; t < e.length; ++t)
                                if (e[t].isIntersecting) {
                                    o();
                                    break
                                }
                        });
                        return t.observe(e),
                            function() {
                                t.disconnect()
                            }
                    }
                    var s = [],
                        l = [c, d].includes(n);
                    l && s.push(a());
                    var f = [c, p].includes(n);
                    f && s.push(r())
                }, b.watchWidgetReset = function() {
                    window.addEventListener("message", function(e) {
                        var t = e.data;
                        t.action && "EappsPlatform.widgetReset" === t.action && b.resetWidget(t.widgetId)
                    })
                }, b.resetWidget = function(e) {
                    var t = function e(t) {
                        var e = document.createElement("div");
                        return e.className = "elfsight-app-" + t, e
                    };
                    b.getWidgetsElements().forEach(function(n) {
                        b.getWidgetIdByElement(n) === e && (delete v[e], n.parentNode.replaceChild(t(e), n))
                    })
                }, b.initWidget = function(e) {
                    var t = b.getWidgetIdByElement(e),
                        i = v[t];
                    if (i) {
                        if (!i.status || !i.data) return void b.logError('Widget "' + t + '" can`t be initialized because ' + i.reason, e);
                        i.data.id = t, i.data.platform = !0;
                        var o = i.user || i.data.user;
                        o && (i.data.isOwner = o.owner), n.initWidget(e, i.data)
                    }
                }, b.boot = function(e, t) {
                    var n = t || w,
                        i = [];
                    if (n.forEach(function(e) {
                            y.includes(e) || (y.push(e), i.push(e))
                        }), i.length) {
                        var r = new XMLHttpRequest,
                            a = b.getPlatformUrl();
                        a += "/p/boot/";
                        var s = o.stringify({
                            w: i.join(","),
                            page: b.getPage()
                        });
                        r.open("get", a + "?" + s), r.withCredentials = !0, r.onload = function() {
                            var t = JSON.parse(r.response);
                            t.status || b.logError("Boot failed because " + t.reason, t.data), v = Object.assign({}, v, t.data.widgets), b.loadAssets(t.data.assets), m.forEach(b.initWidget.bind(b)), y = y.filter(function(e) {
                                return !i.includes(e)
                            }), e && e()
                        }, r.send()
                    }
                }, b.getPage = function() {
                    try {
                        var e = document.location.href;
                        if (u.test(e)) return new URL(e).toString()
                    } catch (e) {}
                }, b.getPlatformUrl = function() {
                    return e.eappsCustomPlatformUrl ? e.eappsCustomPlatformUrl : g
                }, b.revise = function() {
                    var e = w.filter(function(e) {
                        return !(e in v)
                    });
                    e.length > 0 ? b.boot(null, e) : m.forEach(b.initWidget.bind(b))
                }, b.loadAssets = function(t) {
                    t && t.length && t.filter(function(e) {
                        return x.indexOf(e) === -1
                    }).forEach(function(t) {
                        var n = e.document.createElement("script");
                        n.src = t, n.async = !0, n.charset = "UTF-8", e.document.head.appendChild(n), x.push(t)
                    })
                }, b.observe = function() {
                    if (e.MutationObserver && !h) {
                        var t = {
                                childList: !0,
                                subtree: !0,
                                characterData: !0
                            },
                            n = null;
                        h = new MutationObserver(function(e) {
                            var t = function(e) {
                                b.requireWidget(b.getWidgetIdByElement(e)), m.includes(e) || m.push(e)
                            };
                            e.forEach(function(e) {
                                var i = function(e) {
                                    var i = b.getWidgetsElements(e);
                                    i.forEach(t), i.length > 0 && (n && clearTimeout(n), n = setTimeout(function() {
                                        b.revise()
                                    }, 1e3))
                                };
                                Array.prototype.forEach.call(e.addedNodes, i)
                            })
                        }), h.observe(e.document, t)
                    }
                }, b.initialize()
            };
        e.exports = h
    },
    function(e, t, n) {
        ! function(t, n) {
            e.exports = n()
        }("domready",
            function() {
                var e, t = [],
                    n = document,
                    i = n.documentElement.doScroll,
                    o = "DOMContentLoaded",
                    r = (i ? /^loaded|^c/ : /^loaded|^i|^c/).test(n.readyState);
                return r || n.addEventListener(o, e = function() {
                        for (n.removeEventListener(o, e), r = 1; e = t.shift();) e()
                    }),
                    function(e) {
                        r ? setTimeout(e, 0) : t.push(e)
                    }
            })
    },
    function(e, t, n) {
        "use strict";

        function i(e) {
            "@babel/helpers - typeof";
            return (i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
                return typeof e
            } : function(e) {
                return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
            })(e)
        }

        function o(e) {
            switch (e.arrayFormat) {
                case "index":
                    return function(t, n, i) {
                        return null === n ? [a(t, e), "[", i, "]"].join("") : [a(t, e), "[", a(i, e), "]=", a(n, e)].join("")
                    };
                case "bracket":
                    return function(t, n) {
                        return null === n ? a(t, e) : [a(t, e), "[]=", a(n, e)].join("")
                    };
                default:
                    return function(t, n) {
                        return null === n ? a(t, e) : [a(t, e), "=", a(n, e)].join("")
                    }
            }
        }

        function a(e, t) {
            return t.encode ? t.strict ? l(e) : encodeURIComponent(e) : e
        }

        function s(e) {
            return Array.isArray(e) ? e.sort() : "object" === i(e) ? s(Object.keys(e)).sort(function(e, t) {
                return Number(e) - Number(t)
            }).map(function(t) {
                return e[t]
            }) : e
        }
        var l = n(6),
            c = n(7);
        t.extract = function(e) {
            return e.split("?")[1] || ""
        }, t.parse = function(e, t) {
            t = c({
                arrayFormat: "none"
            }, t);
            var n = r(t),
                o = Object.create(null);
            return "string" != typeof e ? o : (e = e.trim().replace(/^(\?|#|&)/, "")) ? (e.split("&").forEach(function(e) {
                var t = e.replace(/\+/g, " ").split("="),
                    i = t.shift(),
                    r = t.length > 0 ? t.join("=") : void 0;
                r = void 0 === r ? null : decodeURIComponent(r), n(decodeURIComponent(i), r, o)
            }), Object.keys(o).sort().reduce(function(e, t) {
                var n = o[t];
                return Boolean(n) && "object" === i(n) && !Array.isArray(n) ? e[t] = s(n) : e[t] = n, e
            }, Object.create(null))) : o
        }, t.stringify = function(e, t) {
            var n = {
                encode: !0,
                strict: !0,
                arrayFormat: "none"
            };
            t = c(n, t);
            var i = o(t);
            return e ? Object.keys(e).sort().map(function(n) {
                var o = e[n];
                if (void 0 === o) return "";
                if (null === o) return a(n, t);
                if (Array.isArray(o)) {
                    var r = [];
                    return o.slice().forEach(function(e) {
                        void 0 !== e && r.push(i(n, e, r.length))
                    }), r.join("&")
                }
                return a(n, t) + "=" + a(o, t)
            }).filter(function(e) {
                return e.length > 0
            }).join("&") : ""
        }
    },
    function(e, t) {
        "use strict";
        e.exports = function(e) {
            return encodeURIComponent(e).replace(/[!'()*]/g, function(e) {
                return "%" + e.charCodeAt(0).toString(16).toUpperCase()
            })
        }
    },
    function(e, t) {
        "use strict";

        function n(e) {
            if (null === e || void 0 === e) throw new TypeError("");
            return Object(e)
        }

        function i() {
            try {
                if (!Object.assign)
                    return !1;
                var e = new String("abc");
                if (e[5] = "de", "5" === Object.getOwnPropertyNames(e)[0]) return !1;
                for (var t = {}, n = 0; n < 10; n++) t["_" + String.fromCharCode(n)] = n;
                var i = Object.getOwnPropertyNames(t).map(function(e) {
                    return t[e]
                });
                if ("0123456789" !== i.join("")) return !1;
                var o = {};
                return "abcdefghijklmnopqrst".split("").forEach(function(e) {
                    o[e] = e
                }), "abcdefghijklmnopqrst" === Object.keys(Object.assign({}, o)).join("")
            } catch (e) {
                return !1
            }
        }
        var o = Object.getOwnPropertySymbols,
            r = Object.prototype.hasOwnProperty,
            a = Object.prototype.propertyIsEnumerable;
        e.exports = i() ? Object.assign : function(e, t) {
            for (var i, s, l = n(e), c = 1; c < arguments.length; c++) {
                i = Object(arguments[c]);
                for (var p in i) r.call(i, p) && (l[p] = i[p]);
                if (o) {
                    s = o(i);
                    for (var d = 0; d < s.length; d++) a.call(i, s[d]) && (l[s[d]] = i[s[d]])
                }
            }
            return l
        }
    },
    function(e, t) {
        function n(e, t, n) {
            var i = [n + ' throws: "' + e + '"'];
            t && (i.push("with \n\t ->"), i.push(t)), console.error.apply(console, i)
        }
        n.withModule = function(e) {
            return function(t, i) {
                return n(t, i, e)
            }
        }, e.exports = n
    },
    function(e, t) {
        var n = function(e) {
            var t = this;
            t.initialize = function() {}, t.requireWidget = function(t) {
                return e.requireWidget(t)
            }, t.resetWidget = function(t) {
                return e.resetWidget(t)
            }, t.initialize()
        };
        e.exports = n
    },
    function(e, t, n) {
        var i = n(8),
            o = n(11),
            r = n(12),
            a = "eapps.AppsManager",
            s = function() {
                var e = this,
                    t = {},
                    n = [],
                    s = [];
                e.initialize = function() {
                    e.logError = i.withModule(a)
                }, e.facade = function() {
                    return new o(e)
                }, e.register = function(n, i) {
                    if (t.name) return void e.logError('Application "' + n + '" is already registered');
                    var o = new i;
                    t[n] = new r(o), e.initWidgetsFromBuffer(n)
                }, e.app = function(e) {
                    return t[e]
                }, e.initWidget = function(t, i) {
                    var o = e.app(i.app);
                    if (o) {
                        if (s.indexOf(t) !== -1) return;
                        s.push(t), o.initWidget(t, i), e.sendExtensionPostMessage(t, i)
                    } else n.push({
                        element: t,
                        config: i,
                        initialized: !1
                    })
                }, e.initWidgetsFromBuffer = function(t) {
                    n && n.length && n.forEach(function(n) {
                        t !== n.config.app || n.initialized || (n.initialized = !0, e.initWidget(n.element, n.config))
                    })
                }, e.sendExtensionPostMessage = function(e, t) {
                    var n = window.self !== window.top ? window.top : window;
                    n.postMessage({
                        method: "postMessagePlatformWidget",
                        data: {
                            settings: t.settings,
                            app_slug: t.app,
                            public_id: t.id,
                            platform: "core"
                        }
                    }, "*")
                }, e.initialize()
            };
        e.exports = s
    },
    function(e, t) {
        var n = function(e) {
            var t = this;
            t.initialize = function() {}, t.register = function(t, n) {
                return e.register(t, n)
            }, t.initialize()
        };
        e.exports = n
    },
    function(e, t) {
        i = function(e) {
            var t = this,
                i = !1,
                o = [];
            t.initialize = function() {
                e.whenReady(t.ready.bind(t))
            }, t.ready = function() {
                i = !0, t.initWidgetsFromBuffer()
            }, t.initWidget = function(n, r) {
                if (i) {
                    r.websiteUrl = window.location.host || "undefined";
                    var a = {
                            widgetId: r.id || null,
                            widgetToken: r.public_widget_token || null,
                            displayDeactivation: !!r.preferences.display_deactivation,
                            deactivationURL: r.preferences.deactivation_url
                        },
                        s = t.getAttributeSettings(n),
                        l = [r.settings, a, s].reduce(function(e, t) {
                            return Object.keys(t).forEach(function(n) {
                                e[n] = t[n]
                            }), e
                        }, {});
                    e.initWidget(n, l, r), r.isOwner && setTimeout(function() {
                        t.initToolbar(n, r)
                    }, 500)
                } else o.push({
                    element: n,
                    config: r,
                    initialized: !1
                })
            }, t.initToolbar = function(e, t) {
                var i = 0;
                t.usageStatus = function() {
                    var e = "green";
                    return i = 100 * t.percentage, i >= 100 && (e = "red"), i >= 90 && i < 100 && (e = "orange"), e
                };

            }, t.initWidgetsFromBuffer = function() {
                o && o.length && o.forEach(function(e) {
                    e.initialized || (e.initialized = !0, t.initWidget(e.element, e.config))
                })
            }, t.initialize(), t.getAttributeSettings = function(e) {
                var t = {},
                    n = "elfsightApp";
                for (var i in e.dataset)
                    if (i.startsWith(n)) {
                        var o = "attribute".concat(i.replace(n, ""));
                        t[o] = e.dataset[i]
                    } return t
            }
        };
        e.exports = i
    }
]);